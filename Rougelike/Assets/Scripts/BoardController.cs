using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BoardController : MonoBehaviour {

    public int colums;
    public int rows;
    
    public GameObject[] floors;
    public GameObject[] outerWalls;
    public GameObject[] wallobstacles;
    public   GameObject[] foodItems;
    public   GameObject[] enemies;
    public GameObject[] players;
    public GameObject[] music;
    public   GameObject exit;
    private int characterSelect;
    private int firstLoad;
    public int difficulty;

    private Transform gameBoard;
    private List<Vector3> obstaclesGrid;
    
	void Awake () {
        obstaclesGrid = new List<Vector3>();
        difficulty = 3;
	}
	
    private void InitalizeObstaclePositions()
    {
        obstaclesGrid.Clear();
      for(int x = 2; x< colums - 2; x++)
      {
        for(int y= 2; y< rows - 2; y++)
        {
         obstaclesGrid.Add(new Vector3(x, y, 0f));   
            
        }
      }
    }
    
    private void SetupGameBoard()
    {
        gameBoard = new GameObject("Game Board").transform;
        
        for(int x = 0; x < colums; x++)
        {
          for(int y = 0; y < rows; y++)
          {
              GameObject selectedTile;
              if( x ==0 || y ==0 || x == colums - 1|| y == rows -1){
                      selectedTile= outerWalls[Random.Range(0, outerWalls.Length)];
          }
          else
          {
              selectedTile= floors[Random.Range(0, floors.Length)];
          }
             GameObject floortile = (GameObject)Instantiate(selectedTile,new Vector3(x, y, 0f), Quaternion.identity);
              floortile.transform.SetParent(gameBoard);
              
          }
            
        }
    }
    
    private void SetRandomObstaclesOnGrid(GameObject[] obstaclesArray,int mini , int maxi)
    {
        int obstacleCount = Random.Range(mini,maxi-1);
        
        if(obstacleCount > obstaclesGrid.Count)
        {
         obstacleCount = obstaclesGrid.Count;   
        }
        
        for(int index = 0;index < obstacleCount;index++)
        {
            GameObject selectedObstacle = obstaclesArray[Random.Range(0,obstaclesArray.Length)];
            
            Instantiate(selectedObstacle, SelectedGridPosition(), Quaternion.identity);
        }
    }
    private void SetPlayersOnGrid(GameObject[] obstaclesArray, int mini, int maxi)
    {
        GameObject selectedObstacle = obstaclesArray[characterSelect];

        Instantiate(selectedObstacle, new Vector3(1,1), Quaternion.identity);
    }
    private Vector3 SelectedGridPosition()
    {
        int randomIndex = Random.Range(0, obstaclesGrid.Count);
        Vector3  randomPosition = obstaclesGrid[randomIndex];
        obstaclesGrid.RemoveAt(randomIndex);
        return randomPosition;
    }
        
    public void SetupLevel(int currentLevel)
    {
       InitalizeObstaclePositions();
       SetupGameBoard();
       SetRandomObstaclesOnGrid(wallobstacles,3,(9 - difficulty));
       SetRandomObstaclesOnGrid(foodItems,(1+difficulty),(4 + (difficulty/2)));
       int enemyCount = (int)Mathf.Log(currentLevel,2);
       SetRandomObstaclesOnGrid(enemies,enemyCount,enemyCount);  
       Instantiate(exit, new Vector3(colums -2, rows-2,0f),Quaternion.identity);
        if(firstLoad == 1)
        {
            SetPlayersOnGrid(players, 1, 1);
        }
    }
    //button stuff
    public void Buttonpushing()
    {
        characterSelect = 0;
        firstLoad = 1;
        SetPlayersOnGrid(players, 1, 1);
        SetPlayersOnGrid(music, 1, 1);
    }
    public void ButtonpushingB()
    {
        characterSelect = 1;
        firstLoad = 1;
        SetPlayersOnGrid(players, 1, 1);
        SetPlayersOnGrid(music, 1, 1);
    }
    public void difficultyeasy()
    {
        difficulty = 4;
        Debug.Log(difficulty);
    }
    public void difficultnormal()
    {
        difficulty = 2;
        Debug.Log(difficulty);  
    }
    public void difficultyhard()
    {
        difficulty = 0;
        Debug.Log(difficulty);
    }
}
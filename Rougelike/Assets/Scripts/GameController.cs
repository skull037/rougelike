using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth;

    private BoardController boardController;
    private List<Enemy> enemies;
    public GameObject LevelImage;
    private Text LevelText;
    private Text HeroBText;
    private Text HeroAText;
    private Text RestartText;
    private Text QuitText;
    private bool settingUpGame;
    private bool mainMenuOn;
    public int secondsUntillevelStart = 20000;
    private int currentLevel = 1;
    public int highScore;
    private int difficulty;
    public Text diffnormal;
    public Text diffeasy;
    public Text diffhard;

    private int turnNumber = 0;

    void Awake()
    {
        difficulty = 2;
        highScore = PlayerPrefs.GetInt("HighestDay");
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

    void Start()
    {
        InitializeGame();
        LevelText.text = "Rouge like game\n Pick difficulty then \n pick your hero's name";
        HeroAText.text = "Brad";
        HeroBText.text = "Chara";
        RestartText.text = "HighScore: ";
        QuitText.text = ""+highScore+" days";
        diffeasy.text = "Easy";
        diffnormal.text = "Normal";
        diffhard.text = "Hard";
    }

    private void InitializeGame()
    {
        boardController = GetComponent<BoardController>();
        settingUpGame = true;
        LevelImage = GameObject.Find("LevelImageo");
        LevelText = GameObject.Find("LevelText").GetComponent<Text>();
        HeroBText = GameObject.Find("playerB").GetComponent<Text>();
        HeroAText = GameObject.Find("playerA").GetComponent<Text>();
        RestartText = GameObject.Find("RestartText").GetComponent<Text>();
        QuitText = GameObject.Find("QuitText").GetComponent<Text>();
        diffeasy = GameObject.Find("difeasy").GetComponent<Text>();
        diffnormal = GameObject.Find("difnormal").GetComponent<Text>();
        diffhard = GameObject.Find("difhard").GetComponent<Text>();
        LevelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        if (mainMenuOn == false)
        {
            settingUpGame = false;
            LevelText.text = "Day " + currentLevel;
            HeroAText.text = "please";
            HeroBText.text = "wait";
            diffeasy.text = "";
            diffnormal.text = "";
            diffhard.text = "";
            RestartText.text = "";
            QuitText.text = "";
            Invoke("DisableLevelImage", secondsUntillevelStart);
        }
    }

    private void DisableLevelImage()
    {
        LevelImage = GameObject.Find("LevelImageo");
        LevelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
        mainMenuOn = false;
    }
    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel +=1;
        InitializeGame();
    }
    void Update()
    {
            if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            mainMenuOn = false;
            return;
        }
        StartCoroutine(MoveEnemies());
        turnNumber++;
    }
    private IEnumerator MoveEnemies()
    {
        secondsUntillevelStart = 2;
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);

        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyTolist(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    public void GameOver()
    {
        HeroAText.text = "";
        HeroBText.text = "";
        RestartText.text = "Restart?";
        QuitText.text = "Quit?";

        if (highScore < currentLevel)
        {
            PlayerPrefs.SetInt("HighestDay", currentLevel);
        }
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        LevelText.text = "You died after " + currentLevel + " days and "+turnNumber+" turns... \n\n" + "High score: " + highScore+ " days";
        LevelImage.SetActive(true);
        //enabled = false;
    }

    private void Buttonpushing()
    {
        secondsUntillevelStart = 2000;
        mainMenuOn = true;
        DisableLevelImage();
    }
    private void ButtonpushingB()
    {
        secondsUntillevelStart = 2000;
        mainMenuOn = true;
        DisableLevelImage();
    }
    public void RestartGame()
    {
        currentLevel = 1;
        boardController = GetComponent<BoardController>();
        boardController.SetupLevel(currentLevel);
        Debug.Log("Restarting");
        InitializeGame();
        Application.LoadLevel(Application.loadedLevel);
        isPlayerTurn = true;
        SoundController.Instance.music.Play();
        enabled = true;
        LevelImage.SetActive(false);
    }
    public void DisableButton()
    {
        //GetComponent<Button>().interactable = false;
    }
    public void quitGame()
    {
        Application.Quit();
    }


}
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Player : MovingObject {    
    
    private Animator animator;
    public int attackPower;
    public int playerHealth;
    public int HealthPerFruit;
    public int HealthPerSoda;
    public int HealthPerCan;
    public int HealthPerMine;
    private int secondsUntilNextLevel=1;

    public Text healthText;
    public AudioClip movementSound1;
    public AudioClip movementSound2;
    public AudioClip chop1;
    public AudioClip chop2;
    public AudioClip fruit1;
    public AudioClip fruit2;
    public AudioClip soda1;
    public AudioClip soda2;
    public AudioClip gameOverSound;

    protected override void Start()
    {
        healthText = GameObject.Find("Texty").GetComponent<Text>();
        base.Start();
        animator = GetComponent<Animator>();
        playerHealth = GameController.Instance.playerCurrentHealth;
        healthText.text = "Health:"+playerHealth;
    }
    
    private void OnDisable()
    {
      GameController.Instance.playerCurrentHealth = playerHealth;   
    }
	void Update () {
        if (!GameController.Instance.isPlayerTurn)
        {
         return;   
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        CheckIfGameOver();
	int xAxis = 0;
    int yAxis = 0;
         
        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");
        
        if(xAxis !=0)
        {
          yAxis = 0;   
        }
        if (yAxis != 0)
        {
            xAxis = 0;
        }
        if (xAxis !=0||yAxis!=0)
        {
         playerHealth--;
         healthText.text ="Health: "+playerHealth;
         SoundController.Instance.PlaySingle(movementSound1,movementSound2);
         Move<Wall>(xAxis,yAxis);
         GameController.Instance.isPlayerTurn = false;
        }
	}
    
    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if(objectPlayerCollidedWith.tag =="Exit")
        {
            Invoke("LoadNewLevel",secondsUntilNextLevel);
            enabled = false;
        }
        
        else if (objectPlayerCollidedWith.tag =="Fruit")
        {
            playerHealth += HealthPerFruit;
        healthText.text ="+"+HealthPerFruit+" Health\n"+"Health: "+playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruit1,fruit2);
        }
        
        else if(objectPlayerCollidedWith.tag =="Soda")
        {
         playerHealth += HealthPerSoda;
        healthText.text ="+"+HealthPerSoda+" Health\n"+"Health: "+playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(soda1,soda2);
        }
         else if (objectPlayerCollidedWith.tag =="Cans")
        {
            playerHealth += HealthPerCan;
        healthText.text ="+"+HealthPerCan+" Health\n"+"Health: "+playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(soda1,fruit1);
        }
        else if (objectPlayerCollidedWith.tag == "Landmine")
        {
            playerHealth -= HealthPerMine;
            healthText.text = "-" + HealthPerMine + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
        }
    }
    
    private void LoadNewLevel()
    {
      Application.LoadLevel(Application.loadedLevel);   
    }
    
    protected override void HandleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("PlayerAttack");
        SoundController.Instance.PlaySingle(chop1,chop2);
        wall.DamageWall(attackPower);
    }

    public void TakeDamage(int damageRecevied)
    {
        playerHealth -= damageRecevied;
        healthText.text ="-"+damageRecevied+" Health\n"+"Health: "+playerHealth;
        animator.SetTrigger("PlayerHurt");
    }
    private void CheckIfGameOver()
    {
     if(playerHealth <=0)
     {
         GameController.Instance.GameOver();
         SoundController.Instance.PlaySingle(gameOverSound);
           playerHealth = 76;
        }
    }
}
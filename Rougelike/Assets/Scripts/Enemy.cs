using UnityEngine;
using System.Collections;

public class Enemy : MovingObject
{

    private Transform player;
    private bool skipCurrentMove;
    private Animator animator;
    public int attackDamage;
    public AudioClip enemySound1;
    public AudioClip enemySound2;
    public int lowChance;
    public int highChance;
    public int lowAttack;
    public int highAttack;
    private int chanceToAttack;
    public int HealthPoints;

    public bool isEnemyStrong;

    protected override void Start()
    {
        GameController.Instance.AddEnemyTolist(this);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        skipCurrentMove = true;
        animator = GetComponent<Animator>();
        base.Start();
    }

    public void MoveEnemy()
    {
        if (skipCurrentMove)
        {
            if (isEnemyStrong == true)
            {
                int chanceToMove = Random.Range(lowChance, highChance);
                if (chanceToMove >= lowChance)
                {
                    skipCurrentMove = false;
                    return;
                }
                else
                {
                    skipCurrentMove = false;
                    return;
                }
            }

        }


        int xAxis = 0;
        int yAxis = 0;

        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);

        if (xAxisDistance > yAxisDistance)
        {
            if (player.position.x > transform.position.x)
            {
                xAxis = 1;
            }

            else
            {
                xAxis = -1;
            }
        }
        else
        {
            if (player.position.y > transform.position.y)
            {
                yAxis = 1;
            }
            else
            {
                yAxis = -1;
            }
        }
        Move<Player>(xAxis, yAxis);
        skipCurrentMove = true;
    }
    protected override void HandleCollision<T>(T component)
    {
        int chanceToAttack = Random.Range(lowAttack, highAttack);
        attackDamage = chanceToAttack;
        Player player = component as Player;
        player.TakeDamage(attackDamage);
        animator.SetTrigger("EnemyAttack");
        SoundController.Instance.PlaySingle(enemySound1, enemySound2);

    }

    public void DamageEnemy(int damageRecevied)
    {
        HealthPoints -= damageRecevied;
        if (HealthPoints <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}